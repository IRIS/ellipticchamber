# -*- coding: utf-8 -*-
"""
Created on Mon Feb 18 18:14:58 2019

@author: mmiglior

Evaluate all the impedances with unit surface impedance

For space charge evaluate space charge impedance

all values are for one meter of structure

WARNING: for circular pipe use small number of terms, 10 are enough
for parallel planes higher, 50 or more.
"""

import sys
sys.path.append('../only_fields')
import matplotlib.pyplot as plt
import numpy as np
import scipy.special as sp
from scipy import constants
import mathiequa as maq
from scipy.special import iv, kv

# Ellipse parameters
b_axis = 0.025 #*************************   MINOR SEMIAXIS!!! ****************
qr = 1.e-12
a_axis = b_axis * (1 + qr)/(1 - qr)
F_ell = np.sqrt(a_axis**2 - b_axis**2)
ecc = F_ell / a_axis
mu0 = np.arccosh(1./ecc)

# Beam parameters
gamma = 27.7
beta = np.sqrt(1-1/gamma**2)
v = beta * constants.c
Z0 = constants.mu_0 * constants.c

Z_l = []
Z_l_SC = []
Z_qx = []
Z_qy = []
Z_qx_SC = []
Z_qy_SC = []
Z_dx = []
Z_dy = []
Z_dx_SC = []
Z_dy_SC = []

frequency1=np.logspace(3,10,50)
frequency2=np.logspace(10,12,50)
frequency=np.append(frequency1,frequency2)
#****************************************** frequency 1 **************************
nterms=5 #5 #30
n_matrix=10 #10 #60

omega = 2 * np.pi * frequency1
k0 = omega / constants.c
kz = omega / v
kt2 = k0**2 - kz**2
q_v = - (kt2 * F_ell**2) / 4

def get_projL(r, t, mu):
    prefactor = np.pi * np.sqrt(2) / sp.gamma(0.5)
    
    abs_rt = np.abs(r - t)
    factor1 = np.exp(-(2 * abs_rt + 1) * mu) * sp.gamma(0.5 + abs_rt) / sp.factorial(abs_rt)
    factor2 = np.exp(-(2*r + 2*t + 1)*mu) * sp.gamma(r+t+0.5) / sp.factorial(r+t)
    
    hyp1 = sp.hyp2f1(0.5, 0.5+abs_rt, 1+abs_rt, np.exp(-4*mu))
    hyp2 = sp.hyp2f1(0.5,0.5+r+t, 1+r+t, np.exp(-4*mu))
    
    coeff_long_quad = prefactor * (factor1 * hyp1 + factor2 * hyp2)
    
  
    factor2 = np.exp(-(2*r + 2*t + 3)*mu) * sp.gamma(r+t+1.5) / sp.factorial(r+t+1)
    hyp2 = sp.hyp2f1(0.5,1.5+r+t, 2+r+t, np.exp(-4*mu))
    
    coeff_xdip = prefactor * (factor1 * hyp1 + factor2 * hyp2)
    coeff_ydip = prefactor * (factor1 * hyp1 - factor2 * hyp2)
   
    return coeff_long_quad, coeff_xdip, coeff_ydip
    
get_projL = np.vectorize(get_projL)
projL_v_lq = np.array([[(-1)**r * (-1)**t *get_projL(r,t,mu0)[0] for t in range(nterms)] for r in range(nterms)])
projL_v_dx = np.array([[(-1)**r * (-1)**t *get_projL(r,t,mu0)[1] for t in range(nterms)] for r in range(nterms)])
projL_v_dy = np.array([[(-1)**r * (-1)**t *get_projL(r,t,mu0)[2] for t in range(nterms)] for r in range(nterms)])


l_v = np.arange(nterms)
for i_fr , freq in enumerate(frequency1):
  
    q = q_v[i_fr]
    mathieu_results = maq.mathieu_results(q, n_matrix)
    A_lq=mathieu_results["results"]["even"]["even"]["expansion_coefficients"]
    A_dy=mathieu_results["results"]["even"]["odd"]["expansion_coefficients"]
    A_dx=mathieu_results["results"]["odd"]["odd"]["expansion_coefficients"]
    
    # longitudinal + quadrupolar
    ce_0=[]
    ce_90=[]
    ce_pp_90_q_neg=[] 
    ce_pp_0=[]
    Ce_mu0=[]
    Ce_0=[]
    Ce_pp_0_q_neg=[]
    Fek_mu0=[]
    
    # dipolar
    ce_0_d=[]
    ce_p_90=[]
    ce_p_90_nq=[]
    Se_mu0=[]
    Ce_mu0_d=[]
    Ce_0_d=[]
    Se_p_0=[]
    se_90=[]
    se_90_nq=[]
    se_p_0=[]    
    Fek_mu0_d=[]
    Gek_mu0_d=[]
    
    for l in np.arange(nterms):
        # longitudinal + quadrupolar
        ce_0.append(maq.solutions.first_kind_azimuthal_ce(0,2*l, mathieu_results))
        ce_90.append(maq.solutions.first_kind_azimuthal_ce(np.pi/2,2*l, mathieu_results))
        ce_pp_0.append(maq.solutions.first_kind_azimuthal_ce_second(0,2*l, mathieu_results)) # q>0
        ce_pp_90_q_neg.append(maq.solutions.first_kind_azimuthal_ce_second_neg_q(np.pi/2,2*l, mathieu_results)) # q<0
        Ce_0.append(maq.solutions.first_kind_azimuthal_ce_neg_q(1j*0, 2*l, mathieu_results)) # q<0
        Ce_pp_0_q_neg.append(-maq.solutions.first_kind_azimuthal_ce_second_neg_q(1j*0,2*l, mathieu_results)) # q<0
        Fek_mu0.append(maq.solutions.second_kind_fek_sumonly(mu0,2*l, mathieu_results)/np.pi) # without any coefficient, only pi!!!
        # dipolar
        ce_0_d.append(maq.solutions.first_kind_azimuthal_ce(0,2*l+1, mathieu_results))
        ce_p_90.append(maq.solutions.first_kind_azimuthal_ce_prime(np.pi/2,2*l+1, mathieu_results))
        ce_p_90_nq.append(maq.solutions.first_kind_azimuthal_ce_prime_neg_q(np.pi/2,2*l+1, mathieu_results))
        Ce_0_d.append(maq.solutions.first_kind_azimuthal_ce_neg_q(1j*0, 2*l+1, mathieu_results)) # q<0
        Se_p_0.append(maq.solutions.first_kind_azimuthal_se_prime_neg_q(1j*0, 2*l+1, mathieu_results)) # q<0
        se_90.append(maq.solutions.first_kind_azimuthal_se(np.pi/2,2*l+1, mathieu_results))
        se_90_nq.append(maq.solutions.first_kind_azimuthal_se_neg_q(np.pi/2,2*l+1, mathieu_results))
        se_p_0.append(maq.solutions.first_kind_azimuthal_se_prime(0,2*l+1, mathieu_results))
        if mu0<10:
            Ce_mu0.append(maq.solutions.first_kind_azimuthal_ce_neg_q(1j*mu0, 2*l, mathieu_results)) # q<0
            Ce_mu0_d.append(maq.solutions.first_kind_azimuthal_ce_neg_q(1j*mu0, 2*l+1, mathieu_results)) # q<0
            Se_mu0.append(-1j*maq.solutions.first_kind_azimuthal_se_neg_q(1j*mu0, 2*l+1, mathieu_results)) # q<0
        else: #use asymptitic expression (Appendix 1 McLachlan 4 (1))
            if A_lq[0,l]==0:
                p1=1e200
            else:
                p1=(-1)**l*ce_0[-1]*ce_90[-1]/A_lq[0,l]
            if A_dx[0,l]==0:
                sp1=1e200
            else:
                sp1=(-1)**l*se_p_0[-1]*se_90[-1]/(np.sqrt(q)*A_dx[0,l])
            if A_dy[0,l]==0:
                pp1=1e200
            else:
                pp1=(-1)**(l+1)*ce_0_d[-1]*ce_p_90[-1]/(np.sqrt(q)*A_dy[0,l])
            Ce_mu0.append(p1*iv(2*l,np.sqrt(q)*np.exp(mu0)))
            Ce_mu0_d.append(sp1*iv(2*l+1,np.sqrt(q)*np.exp(mu0)))
            Se_mu0.append(pp1*iv(2*l+1,np.sqrt(q)*np.exp(mu0)))
        Fek_mu0_d.append(maq.solutions.second_kind_fek_sumonly(mu0,2*l+1, mathieu_results)/np.pi) # without any coefficient, only pi!!!
        Gek_mu0_d.append(maq.solutions.second_kind_gek_sumonly(mu0,2*l+1, mathieu_results)/np.pi) # without any coefficient, only pi!!!
        
        
    # longitudinal + quadrupolar
    ce_0 = np.array(ce_0)
    ce_90 = np.array(ce_90)
    ce_pp_0 = np.array(ce_pp_0)
    ce_pp_90_q_neg = np.array(ce_pp_90_q_neg)
    Ce_mu0 = np.array(Ce_mu0)
    Ce_0 = np.array(Ce_0)
    Ce_pp_0_q_neg = np.array(Ce_pp_0_q_neg)
    Fek_mu0 = np.array(Fek_mu0)
    
    # dipolar
    ce_0_d = np.array(ce_0_d)
    ce_p_90 = np.array(ce_p_90)
    ce_p_90_nq = np.array(ce_p_90_nq)
    Ce_mu0_d = np.array(Ce_mu0_d)
    Ce_0_d = np.array(Ce_0_d)
    Se_mu0 = np.array(Se_mu0)
    Se_p_0 = np.array(Se_p_0)
    se_90 = np.array(se_90)
    se_90_nq = np.array(se_90_nq)
    se_p_0 = np.array(se_p_0)
    Fek_mu0_d = np.array(Fek_mu0_d)
    Gek_mu0_d = np.array(Gek_mu0_d)
    
    # longitudinal + quadrupolar
    A_lq_2r2p = A_lq[0:nterms,0:nterms].T
    A_lq_2t2l = A_lq[0:nterms,0:nterms].T

    sumt = np.dot(A_lq_2t2l,projL_v_lq.T) 	    
    sumr = np.dot(A_lq_2r2p,sumt.T)
    sumpl = np.dot((ce_0*Ce_0/Ce_mu0).T,sumr)
    sumpx = np.dot((-1)**l_v.reshape(1,-1)*(ce_pp_90_q_neg*Ce_0/Ce_mu0).T,sumr)
    sumpy = np.dot((ce_0*Ce_pp_0_q_neg/Ce_mu0).T,sumr)
    sumll = np.dot(((-1)**l_v.reshape(1,-1)*(ce_90*ce_0/Ce_mu0).T),sumpl.T)
    sumlx = np.dot(((-1)**l_v.reshape(1,-1)*(ce_90*ce_0/Ce_mu0).T),sumpx.T)
    sumly = np.dot(((-1)**l_v.reshape(1,-1)*(ce_90*ce_0/Ce_mu0).T),sumpy.T)
    sum_sc_l = np.sum(Fek_mu0*ce_0/Ce_mu0*Ce_0)
    sum_sc_qx = np.sum(Fek_mu0*Ce_0/Ce_mu0*ce_pp_0)
    sum_sc_qy = np.sum(Fek_mu0*ce_0/Ce_mu0*Ce_pp_0_q_neg)
    
    Z_l.append(sumll)
    Z_l_SC.append(sum_sc_l)
    Z_qx.append(sumlx)
    Z_qx_SC.append(sum_sc_qx)
    Z_qy.append(sumly)
    Z_qy_SC.append(sum_sc_qy)

    # Y dipolar 
    A2r2p = A_dy[0:nterms,0:nterms].T
    A2t2l = A_dy[0:nterms,0:nterms].T

    sumt = np.dot(A2t2l,projL_v_dy.T)    
    sumr = np.dot(A2r2p,sumt.T)
    sump = np.dot((ce_0_d*Se_p_0/Se_mu0).T,sumr)
    suml = np.dot(((-1)**l_v.reshape(1,-1)*(ce_p_90*ce_0_d/Se_mu0).T),sump.T)
    sum_sc_dy = np.sum((-1)**l_v.reshape(1,-1)*(se_90_nq*Gek_mu0_d*Se_p_0/Se_mu0))
    Z_dy.append(-suml)
    Z_dy_SC.append(sum_sc_dy)

    # X dipolar
    A2r2p = A_dx[0:nterms,0:nterms].T
    A2t2l = A_dx[0:nterms,0:nterms].T

    sumt = np.dot(A2t2l,projL_v_dx.T)    
    sumr = np.dot(A2r2p,sumt.T)
    sump = np.dot((se_p_0*Ce_0_d/Ce_mu0_d).T,sumr)
    suml = np.dot(((-1)**l_v.reshape(1,-1)*(se_p_0*se_90/Ce_mu0_d).T),sump.T)
    sum_sc_dx = np.sum((-1)**l_v.reshape(1,-1)*(ce_p_90_nq*Fek_mu0_d*Ce_0_d/Ce_mu0_d))
    Z_dx.append(suml)
    Z_dx_SC.append(-sum_sc_dx)


#************************************* frequency 2 *******************************************
nterms=10 #5 #30
n_matrix=20 #10 #60

omega = 2 * np.pi * frequency2
k0 = omega / constants.c
kz = omega / v
kt2 = k0**2 - kz**2
q_v = - (kt2 * F_ell**2) / 4


def get_projL(r, t, mu):
    prefactor = np.pi * np.sqrt(2) / sp.gamma(0.5)
    
    abs_rt = np.abs(r - t)
    factor1 = np.exp(-(2 * abs_rt + 1) * mu) * sp.gamma(0.5 + abs_rt) / sp.factorial(abs_rt)
    factor2 = np.exp(-(2*r + 2*t + 1)*mu) * sp.gamma(r+t+0.5) / sp.factorial(r+t)
    
    hyp1 = sp.hyp2f1(0.5, 0.5+abs_rt, 1+abs_rt, np.exp(-4*mu))
    hyp2 = sp.hyp2f1(0.5,0.5+r+t, 1+r+t, np.exp(-4*mu))
    
    coeff_long_quad = prefactor * (factor1 * hyp1 + factor2 * hyp2)
    
  
    factor2 = np.exp(-(2*r + 2*t + 3)*mu) * sp.gamma(r+t+1.5) / sp.factorial(r+t+1)
    hyp2 = sp.hyp2f1(0.5,1.5+r+t, 2+r+t, np.exp(-4*mu))
    
    coeff_xdip = prefactor * (factor1 * hyp1 + factor2 * hyp2)
    coeff_ydip = prefactor * (factor1 * hyp1 - factor2 * hyp2)
   
    return coeff_long_quad, coeff_xdip, coeff_ydip
    
get_projL = np.vectorize(get_projL)
projL_v_lq = np.array([[(-1)**r * (-1)**t *get_projL(r,t,mu0)[0] for t in range(nterms)] for r in range(nterms)])
projL_v_dx = np.array([[(-1)**r * (-1)**t *get_projL(r,t,mu0)[1] for t in range(nterms)] for r in range(nterms)])
projL_v_dy = np.array([[(-1)**r * (-1)**t *get_projL(r,t,mu0)[2] for t in range(nterms)] for r in range(nterms)])


l_v = np.arange(nterms)
for i_fr , freq in enumerate(frequency2):
  
    q = q_v[i_fr]
    mathieu_results = maq.mathieu_results(q, n_matrix)
    A_lq=mathieu_results["results"]["even"]["even"]["expansion_coefficients"]
    A_dy=mathieu_results["results"]["even"]["odd"]["expansion_coefficients"]
    A_dx=mathieu_results["results"]["odd"]["odd"]["expansion_coefficients"]
    
    # longitudinal + quadrupolar
    ce_0=[]
    ce_90=[]
    ce_pp_90_q_neg=[] 
    ce_pp_0=[]
    Ce_mu0=[]
    Ce_0=[]
    Ce_pp_0_q_neg=[]
    Fek_mu0=[]
    
    # dipolar
    ce_0_d=[]
    ce_p_90=[]
    ce_p_90_nq=[]
    Se_mu0=[]
    Ce_mu0_d=[]
    Ce_0_d=[]
    Se_p_0=[]
    se_90=[]
    se_90_nq=[]
    se_p_0=[]    
    Fek_mu0_d=[]
    Gek_mu0_d=[]
    
    for l in np.arange(nterms):
        # longitudinal + quadrupolar
        ce_0.append(maq.solutions.first_kind_azimuthal_ce(0,2*l, mathieu_results))
        ce_90.append(maq.solutions.first_kind_azimuthal_ce(np.pi/2,2*l, mathieu_results))
        ce_pp_0.append(maq.solutions.first_kind_azimuthal_ce_second(0,2*l, mathieu_results)) # q>0
        ce_pp_90_q_neg.append(maq.solutions.first_kind_azimuthal_ce_second_neg_q(np.pi/2,2*l, mathieu_results)) # q<0
        Ce_0.append(maq.solutions.first_kind_azimuthal_ce_neg_q(1j*0, 2*l, mathieu_results)) # q<0
        Ce_pp_0_q_neg.append(-maq.solutions.first_kind_azimuthal_ce_second_neg_q(1j*0,2*l, mathieu_results)) # q<0
        Fek_mu0.append(maq.solutions.second_kind_fek_sumonly(mu0,2*l, mathieu_results)/np.pi) # without any coefficient, only pi!!!
        # dipolar
        ce_0_d.append(maq.solutions.first_kind_azimuthal_ce(0,2*l+1, mathieu_results))
        ce_p_90.append(maq.solutions.first_kind_azimuthal_ce_prime(np.pi/2,2*l+1, mathieu_results))
        ce_p_90_nq.append(maq.solutions.first_kind_azimuthal_ce_prime_neg_q(np.pi/2,2*l+1, mathieu_results))
        Ce_0_d.append(maq.solutions.first_kind_azimuthal_ce_neg_q(1j*0, 2*l+1, mathieu_results)) # q<0
        Se_p_0.append(maq.solutions.first_kind_azimuthal_se_prime_neg_q(1j*0, 2*l+1, mathieu_results)) # q<0
        se_90.append(maq.solutions.first_kind_azimuthal_se(np.pi/2,2*l+1, mathieu_results))
        se_90_nq.append(maq.solutions.first_kind_azimuthal_se_neg_q(np.pi/2,2*l+1, mathieu_results))
        se_p_0.append(maq.solutions.first_kind_azimuthal_se_prime(0,2*l+1, mathieu_results))
        if mu0<10:
            Ce_mu0.append(maq.solutions.first_kind_azimuthal_ce_neg_q(1j*mu0, 2*l, mathieu_results)) # q<0
            Ce_mu0_d.append(maq.solutions.first_kind_azimuthal_ce_neg_q(1j*mu0, 2*l+1, mathieu_results)) # q<0
            Se_mu0.append(-1j*maq.solutions.first_kind_azimuthal_se_neg_q(1j*mu0, 2*l+1, mathieu_results)) # q<0
        else: #use asymptitic expression (Appendix 1 McLachlan 4 (1))
            if A_lq[0,l]==0:
                p1=1e200
            else:
                p1=(-1)**l*ce_0[-1]*ce_90[-1]/A_lq[0,l]
            if A_dx[0,l]==0:
                sp1=1e200
            else:
                sp1=(-1)**l*se_p_0[-1]*se_90[-1]/(np.sqrt(q)*A_dx[0,l])
            if A_dy[0,l]==0:
                pp1=1e200
            else:
                pp1=(-1)**(l+1)*ce_0_d[-1]*ce_p_90[-1]/(np.sqrt(q)*A_dy[0,l])
            Ce_mu0.append(p1*iv(2*l,np.sqrt(q)*np.exp(mu0)))
            Ce_mu0_d.append(sp1*iv(2*l+1,np.sqrt(q)*np.exp(mu0)))
            Se_mu0.append(pp1*iv(2*l+1,np.sqrt(q)*np.exp(mu0)))
        Fek_mu0_d.append(maq.solutions.second_kind_fek_sumonly(mu0,2*l+1, mathieu_results)/np.pi) # without any coefficient, only pi!!!
        Gek_mu0_d.append(maq.solutions.second_kind_gek_sumonly(mu0,2*l+1, mathieu_results)/np.pi) # without any coefficient, only pi!!!
        
        
    # longitudinal + quadrupolar
    ce_0 = np.array(ce_0)
    ce_90 = np.array(ce_90)
    ce_pp_0 = np.array(ce_pp_0)
    ce_pp_90_q_neg = np.array(ce_pp_90_q_neg)
    Ce_mu0 = np.array(Ce_mu0)
    Ce_0 = np.array(Ce_0)
    Ce_pp_0_q_neg = np.array(Ce_pp_0_q_neg)
    Fek_mu0 = np.array(Fek_mu0)
    
    # dipolar
    ce_0_d = np.array(ce_0_d)
    ce_p_90 = np.array(ce_p_90)
    ce_p_90_nq = np.array(ce_p_90_nq)
    Ce_mu0_d = np.array(Ce_mu0_d)
    Ce_0_d = np.array(Ce_0_d)
    Se_mu0 = np.array(Se_mu0)
    Se_p_0 = np.array(Se_p_0)
    se_90 = np.array(se_90)
    se_90_nq = np.array(se_90_nq)
    se_p_0 = np.array(se_p_0)
    Fek_mu0_d = np.array(Fek_mu0_d)
    Gek_mu0_d = np.array(Gek_mu0_d)
    
    # longitudinal + quadrupolar
    A_lq_2r2p = A_lq[0:nterms,0:nterms].T
    A_lq_2t2l = A_lq[0:nterms,0:nterms].T

    sumt = np.dot(A_lq_2t2l,projL_v_lq.T) 	    
    sumr = np.dot(A_lq_2r2p,sumt.T)
    sumpl = np.dot((ce_0*Ce_0/Ce_mu0).T,sumr)
    sumpx = np.dot((-1)**l_v.reshape(1,-1)*(ce_pp_90_q_neg*Ce_0/Ce_mu0).T,sumr)
    sumpy = np.dot((ce_0*Ce_pp_0_q_neg/Ce_mu0).T,sumr)
    sumll = np.dot(((-1)**l_v.reshape(1,-1)*(ce_90*ce_0/Ce_mu0).T),sumpl.T)
    sumlx = np.dot(((-1)**l_v.reshape(1,-1)*(ce_90*ce_0/Ce_mu0).T),sumpx.T)
    sumly = np.dot(((-1)**l_v.reshape(1,-1)*(ce_90*ce_0/Ce_mu0).T),sumpy.T)
    sum_sc_l = np.sum(Fek_mu0*ce_0/Ce_mu0*Ce_0)
    sum_sc_qx = np.sum(Fek_mu0*Ce_0/Ce_mu0*ce_pp_0)
    sum_sc_qy = np.sum(Fek_mu0*ce_0/Ce_mu0*Ce_pp_0_q_neg)
    
    Z_l.append(sumll)
    Z_l_SC.append(sum_sc_l)
    Z_qx.append(sumlx)
    Z_qx_SC.append(sum_sc_qx)
    Z_qy.append(sumly)
    Z_qy_SC.append(sum_sc_qy)

    # Y dipolar 
    A2r2p = A_dy[0:nterms,0:nterms].T
    A2t2l = A_dy[0:nterms,0:nterms].T

    sumt = np.dot(A2t2l,projL_v_dy.T)    
    sumr = np.dot(A2r2p,sumt.T)
    sump = np.dot((ce_0_d*Se_p_0/Se_mu0).T,sumr)
    suml = np.dot(((-1)**l_v.reshape(1,-1)*(ce_p_90*ce_0_d/Se_mu0).T),sump.T)
    sum_sc_dy = np.sum((-1)**l_v.reshape(1,-1)*(se_90_nq*Gek_mu0_d*Se_p_0/Se_mu0))
    Z_dy.append(-suml)
    Z_dy_SC.append(sum_sc_dy)

    # X dipolar
    A2r2p = A_dx[0:nterms,0:nterms].T
    A2t2l = A_dx[0:nterms,0:nterms].T

    sumt = np.dot(A2t2l,projL_v_dx.T)    
    sumr = np.dot(A2r2p,sumt.T)
    sump = np.dot((se_p_0*Ce_0_d/Ce_mu0_d).T,sumr)
    suml = np.dot(((-1)**l_v.reshape(1,-1)*(se_p_0*se_90/Ce_mu0_d).T),sump.T)
    sum_sc_dx = np.sum((-1)**l_v.reshape(1,-1)*(ce_p_90_nq*Fek_mu0_d*Ce_0_d/Ce_mu0_d))
    Z_dx.append(suml)
    Z_dx_SC.append(-sum_sc_dx)

#*************************************** end frequency loop *****************************
omega = 2 * np.pi * frequency
k0 = omega / constants.c


factor_l = np.sqrt(2.)/(np.pi**2*F_ell)
factor_dip_quad = np.sqrt(2.)*beta/(np.pi**2*F_ell**3*k0)
factor_SC_long=1j*Z0*k0/(beta**2*gamma**2)
factor_SC_dip=1j*Z0*k0/(2.*F_ell*beta**2*gamma**3)
factor_SC_quad=1j*Z0/(beta*gamma**2*F_ell**2)


Z_l=factor_l*np.squeeze(np.asarray(Z_l))
Z_l_SC=factor_SC_long*np.squeeze(np.asarray(Z_l_SC))
Z_qx=factor_dip_quad*np.squeeze(np.asarray(Z_qx))
Z_qx_SC=factor_SC_quad*np.squeeze(np.asarray(Z_qx_SC))
Z_qy=factor_dip_quad*np.squeeze(np.asarray(Z_qy))
Z_qy_SC=factor_SC_quad*np.squeeze(np.asarray(Z_qy_SC))
Z_dy=factor_dip_quad*np.squeeze(np.asarray(Z_dy))
Z_dx=factor_dip_quad*np.squeeze(np.asarray(Z_dx))
Z_dx_SC=factor_SC_dip*np.squeeze(np.asarray(Z_dx_SC)) 
Z_dy_SC=factor_SC_dip*np.squeeze(np.asarray(Z_dy_SC)) 


# round geometry
r0=b_axis
bs_arg=k0*r0/(beta*gamma)
Z_l_round=1./(2*np.pi*r0*iv(0,bs_arg)**2)
Z_l_SC_round=1j*Z0*k0/(2*np.pi*beta**2*gamma**2)*kv(0,bs_arg)/iv(0,bs_arg)
Z_d_round=k0/(4*np.pi*r0*beta*gamma**2*iv(1,bs_arg)**2)
Z_d_SC_round=1j*Z0*k0**2/(4*np.pi*beta**3*gamma**4)*kv(1,bs_arg)/iv(1,bs_arg)
Z_q_round=k0/(4*np.pi*r0*beta*gamma**2*iv(0,bs_arg)**2)
Z_q_SC_round=1j*Z0*k0**2/(4*np.pi*beta**3*gamma**4)*kv(0,bs_arg)/iv(0,bs_arg)

# flat geometry
l_max=5000
const_l=1j*Z0*k0/(2.*np.pi*beta**2*gamma**2)
const_d_q=1j*Z0*k0**2/(4.*np.pi*beta**3*gamma**4)
arg0=k0*b_axis/(beta*gamma)
bsk0=0.
bsk0_p=0.
bsk2=0.
bsk2_p=0.
for n_sum in range(l_max-1):
	l_term=n_sum+1
	bsk0=bsk0+(-1)**l_term*kv(0,arg0*2*l_term)
	bsk0_p=bsk0_p+kv(0,arg0*2*l_term)
	bsk2=bsk2+(-1)**l_term*kv(2,arg0*2*l_term)
	bsk2_p=bsk2_p+kv(2,arg0*2*l_term)
Z_l_SC_flat=-2.*const_l*bsk0
Z_dx_SC_flat=2.*const_d_q*(bsk0-bsk2)
Z_dy_SC_flat=2.*const_d_q*(bsk0_p+bsk2_p)
Z_qx_SC_flat=-Z_dx_SC_flat
Z_qy_SC_flat=-2.*const_d_q*(bsk0+bsk2)



# Plot
plt.close('all')
#*******************************************************************
plt.figure()
plt.suptitle('Longitudinal',size=16)
plt.rcParams['figure.figsize'] = (10, 5)
plt.rcParams['xtick.labelsize']= 12
plt.rcParams['ytick.labelsize']= 12


plt.subplot(1,2,1)
plt.xscale('log')
plt.yscale('symlog')
plt.plot(frequency, np.real(Z_l),color='r',linewidth=2,label='Z_l')
plt.plot(frequency, Z_l_round,'--',color='k',linewidth=2,label='Z_l round')
plt.ylabel("1/m", fontsize=12)
plt.xlabel("$f (Hz)$", fontsize=22)
plt.legend(loc='best')

plt.subplot(1,2,2)
plt.xscale('log')
plt.yscale('linear')
plt.plot(frequency, np.real(Z_l),color='r',linewidth=2,label='Z_l')
plt.plot(frequency, Z_l_round,'--',color='k',linewidth=2,label='Z_l round')
plt.ylabel("1/m", fontsize=12)
plt.xlabel("$f (Hz)$", fontsize=12)
plt.legend(loc='best')
plt.tight_layout()
plt.subplots_adjust(top=0.88)
# ********************************************************************
plt.figure()
plt.suptitle('Longitudinal SC',size=16)
plt.rcParams['figure.figsize'] = (10, 5)
plt.rcParams['xtick.labelsize']= 12
plt.rcParams['ytick.labelsize']= 12

plt.subplot(1,2,1)
plt.xscale('log')
plt.yscale('symlog')
plt.plot(frequency, np.imag(Z_l_SC),color='r',linewidth=2,label='Im(Z_l_SC)')
plt.plot(frequency, np.imag(Z_l_SC_round),'--',color='k',linewidth=2,label='Im(Z_l_SC) round')
plt.plot(frequency, np.imag(Z_l_SC_flat),'--',color='m',linewidth=2,label='Im(Z_l_SC) flat')
plt.ylabel("Ohm/m", fontsize=12)
plt.xlabel("$f (Hz)$", fontsize=22)
plt.legend(loc='best')

plt.subplot(1,2,2)
plt.xscale('log')
plt.yscale('linear')
plt.plot(frequency, np.imag(Z_l_SC),color='r',linewidth=2,label='Im(Z_l_SC)')
plt.plot(frequency, np.imag(Z_l_SC_round),'--',color='k',linewidth=2,label='Im(Z_l_SC) round')
plt.plot(frequency, np.imag(Z_l_SC_flat),'--',color='m',linewidth=2,label='Im(Z_l_SC) flat')
plt.ylabel("Ohm/m", fontsize=12)
plt.xlabel("$f (Hz)$", fontsize=12)
plt.legend(loc='best')
plt.tight_layout()
plt.subplots_adjust(top=0.88)
#*******************************************************************
plt.figure()
plt.suptitle('Dipolar field',size=16)
plt.rcParams['figure.figsize'] = (10, 5)
plt.rcParams['xtick.labelsize']= 12
plt.rcParams['ytick.labelsize']= 12

plt.subplot(1,2,1)
plt.xscale('log')
plt.yscale('log')
plt.plot(frequency, np.real(Z_dx),color='r',linewidth=2,label='Z_dx')
plt.plot(frequency, np.real(Z_dy),color='b',linewidth=2,label='Z_dy')
plt.plot(frequency, Z_d_round,'--',color='k',linewidth=2,label='Z_d round')
plt.ylabel("1/m2", fontsize=12)
plt.xlabel("$f (Hz)$", fontsize=12)
plt.legend(loc='best')

plt.subplot(1,2,2)
plt.xscale('log')
plt.yscale('linear')
plt.plot(frequency, np.real(Z_dx),color='r',linewidth=2,label='Z_dx')
plt.plot(frequency, np.real(Z_dy),color='b',linewidth=2,label='Z_dy')
plt.plot(frequency, Z_d_round,'--',color='k',linewidth=2,label='Z_d round')
plt.ylabel("1/m2", fontsize=12)
plt.xlabel("$f (Hz)$", fontsize=12)
plt.legend(loc='best')
plt.tight_layout()
plt.subplots_adjust(top=0.88)
#*******************************************************************
plt.figure()
plt.suptitle('Dipolar SC',size=16)
plt.rcParams['figure.figsize'] = (10, 5)
plt.rcParams['xtick.labelsize']= 12
plt.rcParams['ytick.labelsize']= 12

plt.subplot(1,2,1)
plt.xscale('log')
plt.yscale('symlog')
plt.plot(frequency, np.imag(Z_dx_SC),color='r',linewidth=2,label='Im(Z_dx_SC)')
plt.plot(frequency, np.imag(Z_dy_SC),color='b',linewidth=2,label='Im(Z_dy_SC)')
plt.plot(frequency, np.imag(Z_d_SC_round),'--',color='k',linewidth=2,label='Im(Z_d_SC) round')
plt.plot(frequency, np.imag(Z_dx_SC_flat),'--',color='m',linewidth=2,label='Im(Z_dx_SC) flat')
plt.plot(frequency, np.imag(Z_dy_SC_flat),'--',color='g',linewidth=2,label='Im(Z_dy_SC) flat')
plt.ylabel("Ohm/m2", fontsize=12)
plt.xlabel("$f (Hz)$", fontsize=12)
plt.legend(loc='best')

plt.subplot(1,2,2)
plt.xscale('log')
plt.yscale('linear')
plt.plot(frequency, np.imag(Z_dx_SC),color='r',linewidth=2,label='Im(Z_dx_SC)')
plt.plot(frequency, np.imag(Z_dy_SC),color='b',linewidth=2,label='Im(Z_dy_SC)')
plt.plot(frequency, np.imag(Z_d_SC_round),'--',color='k',linewidth=2,label='Im(Z_d_SC) round')
plt.plot(frequency, np.imag(Z_dx_SC_flat),'--',color='m',linewidth=2,label='Im(Z_dx_SC) flat')
plt.plot(frequency, np.imag(Z_dy_SC_flat),'--',color='g',linewidth=2,label='Im(Z_dy_SC) flat')
plt.ylabel("Ohm/m2", fontsize=12)
plt.xlabel("$f (Hz)$", fontsize=12)
plt.legend(loc='best')
plt.tight_layout()
plt.subplots_adjust(top=0.88)
#*******************************************************************
plt.figure()
plt.suptitle('Quadrupolar field',size=16)
plt.rcParams['figure.figsize'] = (10, 5)
plt.rcParams['xtick.labelsize']= 12
plt.rcParams['ytick.labelsize']= 12

plt.subplot(1,2,1)
plt.xscale('log')
plt.yscale('symlog')
plt.plot(frequency, np.real(Z_qx),color='r',linewidth=2,label='Z_qx')
plt.plot(frequency, np.real(Z_qy),color='b',linewidth=2,label='Z_qy')
plt.plot(frequency, Z_q_round,'--',color='k',linewidth=2,label='Z_q round')
plt.ylabel("1/m2", fontsize=12)
plt.xlabel("$f (Hz)$", fontsize=12)
plt.legend(loc='best')

plt.subplot(1,2,2)
plt.xscale('log')
plt.yscale('linear')
plt.plot(frequency, np.real(Z_qx),color='r',linewidth=2,label='Z_qx')
plt.plot(frequency, np.real(Z_qy),color='b',linewidth=2,label='Z_qy')
plt.plot(frequency, Z_q_round,'--',color='k',linewidth=2,label='Z_q round')
plt.ylabel("1/m2", fontsize=12)
plt.xlabel("$f (Hz)$", fontsize=12)
plt.legend(loc='best')
plt.tight_layout()
plt.subplots_adjust(top=0.88)
#*******************************************************************
plt.figure()
plt.suptitle('Quadrupolar SC',size=16)
plt.rcParams['figure.figsize'] = (10, 5)
plt.rcParams['xtick.labelsize']= 12
plt.rcParams['ytick.labelsize']= 12

plt.subplot(1,2,1)
plt.xscale('log')
plt.yscale('symlog')
plt.plot(frequency, np.imag(Z_qx_SC),color='r',linewidth=2,label='Im(Z_qx_SC)')
plt.plot(frequency, np.imag(Z_qy_SC),color='b',linewidth=2,label='Im(Z_qy_SC)')
plt.plot(frequency, np.imag(Z_q_SC_round),'--',color='k',linewidth=2,label='Im(Z_q_SC) round')
plt.plot(frequency, np.imag(Z_qx_SC_flat),'--',color='m',linewidth=2,label='Im(Z_qx_SC) flat')
plt.plot(frequency, np.imag(Z_qy_SC_flat),'--',color='g',linewidth=2,label='Im(Z_qy_SC) flat')
plt.ylabel("Ohm/m2", fontsize=12)
plt.xlabel("$f (Hz)$", fontsize=12)
plt.legend(loc='best')

plt.subplot(1,2,2)
plt.xscale('log')
plt.yscale('linear')
plt.plot(frequency, np.imag(Z_qx_SC),color='r',linewidth=2,label='Im(Z_qx_SC)')
plt.plot(frequency, np.imag(Z_qy_SC),color='b',linewidth=2,label='Im(Z_qy_SC)')
plt.plot(frequency, np.imag(Z_q_SC_round),'--',color='k',linewidth=2,label='Im(Z_q_SC) round')
plt.plot(frequency, np.imag(Z_qx_SC_flat),'--',color='m',linewidth=2,label='Im(Z_qx_SC) flat')
plt.plot(frequency, np.imag(Z_qy_SC_flat),'--',color='g',linewidth=2,label='Im(Z_qy_SC) flat')
plt.ylabel("Ohm/m2", fontsize=12)
plt.xlabel("$f (Hz)$", fontsize=12)
plt.legend(loc='best')
plt.tight_layout()
plt.subplots_adjust(top=0.88)


# *********************************** SAVE ********************************
head='f (Hz)          Im(Z_l) (Ohm/m)   Im(Z_dx) (Ohm/m2) Im(Z_dy) (Ohm/m2)  '
head=head+'Im(Z_qx) (Ohm/m2)  Im(Z_qy) (Ohm/m2)'
np.savetxt('imp_SC.txt', list(zip(frequency,np.imag(Z_l_SC),np.imag(Z_dx_SC),
    np.imag(Z_dy_SC),np.imag(Z_qx_SC),np.imag(Z_qy_SC))), fmt='%.9e', 
    header=head, delimiter='   ')

head='f (Hz)            Z_l/Zs (1/m)     Z_dx/Zs (1/m2)    Z_dy/Zs (1/m2)    '
head=head+'Z_qx/Zs (1/m2)     Z_qy/Zs (1/m2)'
np.savetxt('fields_RW.txt', list(zip(frequency,np.real(Z_l),np.real(Z_dx),
    np.real(Z_dy),np.real(Z_qx),np.real(Z_qy))), fmt='%.9e', 
    header=head, delimiter='   ')

    
plt.show()

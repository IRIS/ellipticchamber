import numpy as np
import scipy.linalg as la
from scipy.linalg import eigvalsh_tridiagonal, eigvalsh
import sys

class equation_matrix:

    def __init__(self, q=1., matrix_size=10, function_type='even', order_type='even'):
        self.matrix_size = matrix_size
        self.function_type = function_type
        self.order_type = order_type
        self.q = q

    def build_trdiagonal_matrix(self):
        # Generate interval from 0 to matrix_size, doubled and squared
        diagonal_range_squared = np.zeros((self.matrix_size, self.matrix_size), dtype=complex)
        double_diag_q = np.zeros((self.matrix_size, self.matrix_size), dtype=complex)
        if (self.function_type == 'even'):
            if (self.order_type == 'even'):
                np.fill_diagonal(diagonal_range_squared, (2*np.arange(0, self.matrix_size))**2)
            else:  # odd order
                np.fill_diagonal(diagonal_range_squared, (2*np.arange(0, self.matrix_size) + 1)**2)
                diagonal_range_squared[0, 0] = 1+self.q
        else:  # odd function
            if (self.order_type == 'even'):
                np.fill_diagonal(diagonal_range_squared, (2*np.arange(1, self.matrix_size+1))**2)
            else:  # odd order
                np.fill_diagonal(diagonal_range_squared, (2*np.arange(0, self.matrix_size)+1)**2)
                diagonal_range_squared[0, 0] = 1-self.q
        # Generate q array
        double_diag_q[0,1] = self.q
        double_diag_q[self.matrix_size-1,self.matrix_size-2] = self.q
        double_diag_q[self.matrix_size-2,self.matrix_size-1] = self.q
        for i in range(1,self.matrix_size-1):
            double_diag_q[i,i-1] = self.q
            double_diag_q[i,i+1] = self.q
        # Divide terms by sqrt(2)
        if (self.function_type == 'even' and self.order_type == 'even'):
            double_diag_q[1, 0] = self.q * np.sqrt(2)
            double_diag_q[0, 1] = self.q * np.sqrt(2)
        # Assemble diagonal
        self.tridiagonal_matrix = double_diag_q + diagonal_range_squared

    def compute_eig_all(self):
        #self.mathieu_characteristic, self.expansion_coefficients = la.eig(self.tridiagonal_matrix)
        self.mathieu_characteristic, self.expansion_coefficients = la.eigh_tridiagonal(np.real(np.diagonal(self.tridiagonal_matrix, offset=0)),np.real(np.diagonal(self.tridiagonal_matrix, offset=1)), lapack_driver='stev',  tol=1e-50  )
        
        
	
	
	if (self.function_type == 'even' and self.order_type == 'even'):
            self.expansion_coefficients[0,:] = self.expansion_coefficients[0,:]/np.sqrt(2)
        
            if self.q < 1e-8:
                print('Switching to low-q asymtotic expansions (to 2nd order)')
                self.expansion_coefficients=np.zeros((self.matrix_size,self.matrix_size))
                self.expansion_coefficients[0,0]=1./np.sqrt(2)
                self.expansion_coefficients[1,0]=-self.q/(2*np.sqrt(2))
                self.expansion_coefficients[2,0]=self.q**2/(32*np.sqrt(2))
                
                self.expansion_coefficients[0,1]=self.q/4
                self.expansion_coefficients[1,1]=1.
                self.expansion_coefficients[2,1]=-self.q/12
                self.expansion_coefficients[3,1]=self.q**2/384

                n_index=self.matrix_size-2
                n_index1=n_index+1
                self.expansion_coefficients[n_index-2,n_index]=self.q**2/(32*(2*n_index-1)*(2*n_index-2))
                self.expansion_coefficients[n_index1-2,n_index1]=self.q**2/(32*(2*n_index1-1)*(2*n_index1-2))
                self.expansion_coefficients[n_index-1,n_index]=self.q/(4*(2*n_index-1))
                self.expansion_coefficients[n_index1-1,n_index1]=self.q/(4*(2*n_index1-1))
                self.expansion_coefficients[n_index,n_index]=1.
                self.expansion_coefficients[n_index1,n_index1]=1.
                self.expansion_coefficients[n_index+1,n_index]=-self.q/(4*(2*n_index+1))


                if self.matrix_size > 4:
                    for i in range(self.matrix_size-4):
                        n_index=i+2
                        self.expansion_coefficients[n_index-2,n_index]=self.q**2/(32*(2*n_index-1)*(2*n_index-2))
                        self.expansion_coefficients[n_index-1,n_index]=self.q/(4*(2*n_index-1))
                        self.expansion_coefficients[n_index,n_index]=1.
                        self.expansion_coefficients[n_index+1,n_index]=-self.q/(4*(2*n_index+1))
                        self.expansion_coefficients[n_index+2,n_index]=self.q**2/(32*(2*n_index+1)*(2*n_index+2))
            
        if (self.function_type == 'odd' and self.order_type == 'odd'):
            if self.q < 1e-8:
                print('Switching to low-q asymtotic expansions (to 2nd order)')
                self.expansion_coefficients=np.zeros((self.matrix_size,self.matrix_size))
                self.expansion_coefficients[0,0]=1.
                self.expansion_coefficients[1,0]=-self.q/8+self.q**2/64
                self.expansion_coefficients[2,0]=self.q**2/192

                n_index=self.matrix_size-2
                n_index1=n_index+1
                self.expansion_coefficients[n_index-2,n_index]=self.q**2/(32*(2*n_index-1)*2*n_index)
                self.expansion_coefficients[n_index1-2,n_index1]=self.q**2/(32*(2*n_index1-1)*2*n_index1)
                self.expansion_coefficients[n_index-1,n_index]=self.q/(4*2*n_index)
                self.expansion_coefficients[n_index1-1,n_index1]=self.q/(4*2*n_index1)
                self.expansion_coefficients[n_index,n_index]=1.
                self.expansion_coefficients[n_index1,n_index1]=1.
                self.expansion_coefficients[n_index+1,n_index]=-self.q/(4*(2*n_index+2))


                if self.matrix_size > 3:
                    for i in range(self.matrix_size-3):
                        n_index=i+1
                        self.expansion_coefficients[n_index-2,n_index]=self.q**2/(32*2*n_index*(2*n_index-1))
                        self.expansion_coefficients[n_index-1,n_index]=self.q/(4*2*n_index)
                        self.expansion_coefficients[n_index,n_index]=1.
                        self.expansion_coefficients[n_index+1,n_index]=-self.q/(4*(2*n_index+2))
                        self.expansion_coefficients[n_index+2,n_index]=self.q**2/(32*(2*n_index+2)*(2*n_index+3))
        
        if (self.function_type == 'even' and self.order_type == 'odd'):
            if self.q < 1e-8:
                print('Switching to low-q asymtotic expansions (to 2nd order)')
                self.expansion_coefficients=np.zeros((self.matrix_size,self.matrix_size))
                self.expansion_coefficients[0,0]=1.
                self.expansion_coefficients[1,0]=-self.q/8+self.q**2/64
                self.expansion_coefficients[2,0]=self.q**2/192

                n_index=self.matrix_size-2
                n_index1=n_index+1
                self.expansion_coefficients[n_index-2,n_index]=self.q**2/(32*(2*n_index-1)*2*n_index)
                self.expansion_coefficients[n_index1-2,n_index1]=self.q**2/(32*(2*n_index1-1)*2*n_index1)
                self.expansion_coefficients[n_index-1,n_index]=self.q/(4*2*n_index)
                self.expansion_coefficients[n_index1-1,n_index1]=self.q/(4*2*n_index1)
                self.expansion_coefficients[n_index,n_index]=1.
                self.expansion_coefficients[n_index1,n_index1]=1.
                self.expansion_coefficients[n_index+1,n_index]=-self.q/(4*(2*n_index+2))


                if self.matrix_size > 3:
                    for i in range(self.matrix_size-3):
                        n_index=i+1
                        self.expansion_coefficients[n_index-2,n_index]=self.q**2/(32*2*n_index*(2*n_index-1))
                        self.expansion_coefficients[n_index-1,n_index]=self.q/(4*2*n_index)
                        self.expansion_coefficients[n_index,n_index]=1.
                        self.expansion_coefficients[n_index+1,n_index]=-self.q/(4*(2*n_index+2))
                        self.expansion_coefficients[n_index+2,n_index]=self.q**2/(32*(2*n_index+2)*(2*n_index+3))
 
 
        if (self.function_type == 'odd' and self.order_type == 'even'):
            if self.q < 1e-8:
                print('Switching to low-q asymtotic expansions (to 2nd order)')
                self.expansion_coefficients=np.zeros((self.matrix_size,self.matrix_size))
                self.expansion_coefficients[0,0]=1.
                self.expansion_coefficients[1,0]=-self.q/12
                self.expansion_coefficients[2,0]=self.q**2/384

                n_index=self.matrix_size-2
                n_index1=n_index+1
                self.expansion_coefficients[n_index-2,n_index]=self.q**2/(32*(2*n_index+1)*2*n_index)
                self.expansion_coefficients[n_index1-2,n_index1]=self.q**2/(32*(2*n_index1+1)*2*n_index1)
                self.expansion_coefficients[n_index-1,n_index]=self.q/(4*(2*n_index+1))
                self.expansion_coefficients[n_index1-1,n_index1]=self.q/(4*(2*n_index1-1))
                self.expansion_coefficients[n_index,n_index]=1.
                self.expansion_coefficients[n_index1,n_index1]=1.
                self.expansion_coefficients[n_index+1,n_index]=-self.q/(4*(2*n_index+3))


                if self.matrix_size > 3:
                    for i in range(self.matrix_size-3):
                        n_index=i+1
                        self.expansion_coefficients[n_index-2,n_index]=self.q**2/(32*(2*n_index+1)*2*n_index)
                        self.expansion_coefficients[n_index-1,n_index]=self.q/(4*(2*n_index+1))
                        self.expansion_coefficients[n_index,n_index]=1.
                        self.expansion_coefficients[n_index+1,n_index]=-self.q/(4*(2*n_index+3))
                        self.expansion_coefficients[n_index+2,n_index]=self.q**2/(32*(2*n_index+3)*(2*n_index+4))



    def sort_eig_all(self):
        # Sort eigenvalues and eigenvectors starting from real values of eigenvalues
        sorting_order = np.argsort(self.mathieu_characteristic)
        self.mathieu_characteristic = self.mathieu_characteristic[sorting_order]
        self.expansion_coefficients = self.expansion_coefficients[sorting_order]

    def apply_normalization(self):
        prod_conj = np.conj(self.expansion_coefficients)*self.expansion_coefficients
        self.expansion_coefficients_conj = np.sum(prod_conj,0)
        if (self.function_type == 'even' and self.order_type == 'even'):
            self.expansion_coefficients_conj += np.conj(self.expansion_coefficients[0,:])*self.expansion_coefficients[0,:]
        if np.iscomplex(self.q):
            self.expansion_coefficients=np.multiply(np.outer(np.ones((self.matrix_size,1),dtype=complex),
                1/np.sqrt(self.expansion_coefficients_conj)),self.expansion_coefficients)
        else:  # 'q' is real
            exp_coeff_sign = np.zeros(self.matrix_size)
            n_range = np.zeros(self.matrix_size)     
            #
            if (self.function_type == 'even'):
                exp_coeff_sign = np.sign(np.sum(self.expansion_coefficients,0))           
            else:
                if (self.order_type == 'even'):
                    n_range = np.arange(2,self.matrix_size*2+1,2)
                else:
                    n_range = np.arange(1,2*self.matrix_size,2)
                exp_coeff_sign = np.sign(np.dot(n_range,self.expansion_coefficients))           
            exp_coeff_sign += (exp_coeff_sign == 0).astype(np.int)
            self.expansion_coefficients_conj = np.divide(exp_coeff_sign, np.sqrt(self.expansion_coefficients_conj))
        self.expansion_coefficients *= np.vstack([self.expansion_coefficients_conj]*self.matrix_size)

    def function_coefficients(self):
        self.build_trdiagonal_matrix()
        self.compute_eig_all()
        self.sort_eig_all()
        self.apply_normalization()
        mathieu_data = {"expansion_coefficients": self.expansion_coefficients, "characteristic_values": self.mathieu_characteristic, "tridiagonal_matrix": self.tridiagonal_matrix}
        return (mathieu_data)



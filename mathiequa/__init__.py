"""This module compute Mathieu solutions"""

from mathiequa.equation_matrix import equation_matrix
from mathiequa.solutions import first_kind_azimuthal_ce, first_kind_azimuthal_ce_prime
from mathiequa.solutions import first_kind_azimuthal_se, first_kind_azimuthal_se_prime
from mathiequa.solutions import first_kind_radial_ce, first_kind_radial_ce_prime
from mathiequa.solutions import first_kind_radial_se, first_kind_radial_se_prime
from mathiequa.solutions import mathieu_results
from mathiequa.solutions import second_kind_fek_sumonly
from mathiequa.solutions import second_kind_gek_sumonly

__all__ = ['equation_matrix', 'first_kind_azimuthal_ce', 'first_kind_azimuthal_ce_prime', 'first_kind_azimuthal_se', 'first_kind_azimuthal_se_prime',
    'second_kind_fek_sumonly','second_kind_gek_sumonly', 'first_kind_radial_ce', 'first_kind_radial_se',
    'first_kind_radial_ce_prime', 'first_kind_radial_se_prime', 'mathieu_results']

import numpy as np
import scipy.linalg as la
import scipy.special as scspec
import sys
import mathiequa


def mathieu_results(q, matrix_size): #q>0
    all_data = {}
    all_data["results"] = {}
    all_data["results"]["even"] = {}
    all_data["results"]["odd"] = {}
    all_data["results"]["even"]["even"] = mathiequa.equation_matrix(q=q, matrix_size=matrix_size, function_type='even', order_type='even').function_coefficients()
    all_data["results"]["even"]["odd"] = mathiequa.equation_matrix(q=q, matrix_size=matrix_size, function_type='even', order_type='odd').function_coefficients()
    all_data["results"]["odd"]["even"] = mathiequa.equation_matrix(q=q, matrix_size=matrix_size, function_type='odd', order_type='even').function_coefficients()
    all_data["results"]["odd"]["odd"] = mathiequa.equation_matrix(q=q, matrix_size=matrix_size, function_type='odd', order_type='odd').function_coefficients()
    all_data["matrix_size"]=matrix_size
    all_data["q"]=q
    return all_data

def first_kind_azimuthal_ce(value, order, mathieu_results): 
    matrix_size = mathieu_results["matrix_size"]
    if order % 2 == 0:
        exp_coeff = mathieu_results["results"]["even"]["even"]["expansion_coefficients"]
        n = 2*np.arange(0, matrix_size)
    else:
        exp_coeff = mathieu_results["results"]["even"]["odd"]["expansion_coefficients"]
        n = 1+2*np.arange(0, matrix_size)
    ret_value = np.dot(np.cos(value*n), exp_coeff[:, int(np.fix(order/2))])
    return np.nan_to_num(ret_value)

def first_kind_azimuthal_ce_neg_q(value, order, mathieu_results): 
    matrix_size = mathieu_results["matrix_size"]
    if order % 2 == 0:
        exp_coeff = mathieu_results["results"]["even"]["even"]["expansion_coefficients"]
        n = 2*np.arange(0, matrix_size)
    else:
        exp_coeff = mathieu_results["results"]["odd"]["odd"]["expansion_coefficients"]
        n = 1+2*np.arange(0, matrix_size)
    ret_value = np.power(-1, int(np.fix(order/2)))*np.dot(np.cos(value*n), \
    	np.power(-1, np.fix(n/2))*exp_coeff[:, int(np.fix(order/2))])
    return np.nan_to_num(ret_value)


def first_kind_azimuthal_ce_prime(value, order, mathieu_results):
    matrix_size = mathieu_results["matrix_size"]
    if order % 2 == 0:
        exp_coeff = mathieu_results["results"]["even"]["even"]["expansion_coefficients"]
        n = 2*np.arange(0, matrix_size)
    else:
        exp_coeff = mathieu_results["results"]["even"]["odd"]["expansion_coefficients"]
        n = 1+2*np.arange(0, matrix_size)
    ret_value = -np.dot(np.sin(value*n), n*exp_coeff[:, int(np.fix(order/2))])
    return np.nan_to_num(ret_value)

def first_kind_azimuthal_ce_prime_neg_q(value, order, mathieu_results): #q<0
    matrix_size = mathieu_results["matrix_size"]
    if order % 2 == 0:
        exp_coeff = mathieu_results["results"]["even"]["even"]["expansion_coefficients"]
        n = 2*np.arange(0, matrix_size)
    else:
        exp_coeff = mathieu_results["results"]["odd"]["odd"]["expansion_coefficients"]
        n = 1+2*np.arange(0, matrix_size)
    ret_value = -np.power(-1, int(np.fix(order/2)))*np.dot(np.sin(value*n), \
		np.power(-1, np.fix(n/2))*n*exp_coeff[:, int(np.fix(order/2))])
    return np.nan_to_num(ret_value)

def first_kind_azimuthal_ce_second(value, order, mathieu_results):
    matrix_size = mathieu_results["matrix_size"]
    if order % 2 == 0:
        exp_coeff = mathieu_results["results"]["even"]["even"]["expansion_coefficients"]
        n = 2*np.arange(0, matrix_size)
    else:
        exp_coeff = mathieu_results["results"]["even"]["odd"]["expansion_coefficients"]
        n = 1+2*np.arange(0, matrix_size)
    ret_value = -np.dot(np.cos(value*n), n**2*exp_coeff[:, int(np.fix(order/2))])
    return np.nan_to_num(ret_value)


def first_kind_azimuthal_ce_second_neg_q(value, order, mathieu_results): #q<0
    matrix_size = mathieu_results["matrix_size"]
    if order % 2 == 0:
        exp_coeff = mathieu_results["results"]["even"]["even"]["expansion_coefficients"]
        n = 2*np.arange(0, matrix_size)
    else:
        exp_coeff = mathieu_results["results"]["odd"]["odd"]["expansion_coefficients"]
        n = 1+2*np.arange(0, matrix_size)
    ret_value = -np.power(-1, int(np.fix(order/2)))*np.dot(np.cos(value*n), \
		np.power(-1, np.fix(n/2))*n**2*exp_coeff[:, int(np.fix(order/2))])
    return np.nan_to_num(ret_value)


def first_kind_azimuthal_se(value, order, mathieu_results):
    matrix_size = mathieu_results["matrix_size"]
    if order % 2 == 0:
        exp_coeff = mathieu_results["results"]["odd"]["even"]["expansion_coefficients"]
        n = 2*np.arange(1, matrix_size+1)
    else:
        exp_coeff = mathieu_results["results"]["odd"]["odd"]["expansion_coefficients"]
        n = 1+2*np.arange(0, matrix_size)
    ret_value = np.dot(np.sin(value*n), exp_coeff[:, int(np.fix(order/2))])
    return np.nan_to_num(ret_value)

def first_kind_azimuthal_se_neg_q(value, order, mathieu_results): # q<0
    matrix_size = mathieu_results["matrix_size"]
    if order % 2 == 0:
        exp_coeff = mathieu_results["results"]["odd"]["even"]["expansion_coefficients"]
        n = 2*np.arange(1, matrix_size+1)
    else:
        exp_coeff = mathieu_results["results"]["even"]["odd"]["expansion_coefficients"]
        n = 1+2*np.arange(0, matrix_size)
    ret_value = np.power(-1, int(np.fix(order/2)))*np.dot(np.sin(value*n), \
    	np.power(-1, np.fix(n/2))*exp_coeff[:, int(np.fix(order/2))])
    return np.nan_to_num(ret_value)


def first_kind_azimuthal_se_prime(value, order, mathieu_results):
    matrix_size = mathieu_results["matrix_size"]
    if order % 2 == 0:
        exp_coeff = mathieu_results["results"]["odd"]["even"]["expansion_coefficients"]
        n = 2*np.arange(1, matrix_size+1)
    else:
        exp_coeff = mathieu_results["results"]["odd"]["odd"]["expansion_coefficients"]
        n = 2*np.arange(1, matrix_size+1)-1
    ret_value = np.dot(np.cos(value*n), np.multiply(n,exp_coeff[:, int(np.fix(order/2))]))
    return np.nan_to_num(ret_value)

def first_kind_azimuthal_se_prime_neg_q(value, order, mathieu_results): # q<0
    matrix_size = mathieu_results["matrix_size"]
    if order % 2 == 0:
        exp_coeff = mathieu_results["results"]["odd"]["even"]["expansion_coefficients"]
        n = 2*np.arange(1, matrix_size+1)
    else:
        exp_coeff = mathieu_results["results"]["even"]["odd"]["expansion_coefficients"]
        n = 1+2*np.arange(0, matrix_size)
    ret_value = np.power(-1, int(np.fix(order/2)))*np.dot(np.cos(value*n), \
    	np.power(-1, np.fix(n/2))*n*exp_coeff[:, int(np.fix(order/2))])
    return np.nan_to_num(ret_value)


def first_kind_radial_ce(value, order, mathieu_results):
    return first_kind_azimuthal_ce(1j*value, order, mathieu_results)

def first_kind_radial_ce_prime(value, order, mathieu_results):
    ret_value=1j*first_kind_azimuthal_ce_prime(1j*value, order, mathieu_results)
    return ret_value

def first_kind_radial_se(value, order, mathieu_results):
    return first_kind_azimuthal_se(1j*value, order, mathieu_results)

def first_kind_radial_se_prime(value, order, mathieu_results):
    ret_value=1j*first_kind_azimuthal_se_prime(1j*value, order, mathieu_results)
    return ret_value

def second_kind_fek_sumonly(value, order, mathieu_results):
    matrix_size = mathieu_results["matrix_size"]
    nu_1 = np.sqrt(mathieu_results["q"])*np.exp(-value)
    nu_2 = np.sqrt(mathieu_results["q"])*np.exp(value)
    final_scaling = np.exp(abs(np.real(nu_1))-nu_2)
    # final_scaling = np.exp(-np.sqrt(mathieu_results["q"])*2*np.sinh(value))
    if order % 2 == 0:
        exp_coeff = mathieu_results["results"]["even"]["even"]["expansion_coefficients"]
        n = np.arange(0, matrix_size)
        bessel_intermediate = np.multiply(np.nan_to_num(scspec.ive(n, nu_1)), np.nan_to_num(scspec.kve(n, nu_2)))
    else:
        exp_coeff = mathieu_results["results"]["odd"]["odd"]["expansion_coefficients"]
        n = np.arange(0, matrix_size)
        n_plus_one = 1+np.arange(0, matrix_size)
        bessel_intermediate = np.multiply(np.nan_to_num(scspec.ive(n, nu_1)), np.nan_to_num(scspec.kve(n_plus_one, nu_2))) - np.multiply(np.nan_to_num(scspec.ive(n_plus_one, nu_1)), np.nan_to_num(scspec.kve(n, nu_2)))
    ret_value = np.dot(exp_coeff[:, int(np.fix(order/2))], bessel_intermediate)*final_scaling
    return np.nan_to_num(ret_value)

def second_kind_gek_sumonly(value, order, mathieu_results):
    matrix_size = mathieu_results["matrix_size"]
    nu_1 = np.sqrt(mathieu_results["q"])*np.exp(-value)
    nu_2 = np.sqrt(mathieu_results["q"])*np.exp(value)
    final_scaling = np.exp(abs(np.real(nu_1))-nu_2)
    # final_scaling = np.exp(-np.sqrt(mathieu_results["q"])*2*np.sinh(value))
    if order % 2 == 0:
        exp_coeff = mathieu_results["results"]["odd"]["even"]["expansion_coefficients"]
        n = np.arange(0, matrix_size)
        n_plus_one = 1+np.arange(0, matrix_size)
        bessel_intermediate = np.multiply(np.nan_to_num(scspec.ive(n, nu_1)), np.nan_to_num(scspec.kve(n_plus_one, nu_2))) + np.multiply(np.nan_to_num(scspec.ive(n_plus_one, nu_1)), np.nan_to_num(scspec.kve(n, nu_2)))
    else:
        exp_coeff = mathieu_results["results"]["even"]["odd"]["expansion_coefficients"]
        n = np.arange(0, matrix_size)
        n_plus_one = 1+np.arange(0, matrix_size)
        bessel_intermediate = np.multiply(np.nan_to_num(scspec.ive(n, nu_1)), np.nan_to_num(scspec.kve(n_plus_one, nu_2))) + np.multiply(np.nan_to_num(scspec.ive(n_plus_one, nu_1)), np.nan_to_num(scspec.kve(n, nu_2)))
    ret_value = np.dot(exp_coeff[:, int(np.fix(order/2))], bessel_intermediate)*final_scaling
    return np.nan_to_num(ret_value)
